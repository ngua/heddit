#+TITLE: Getting Started

The following is a brief guide to getting started with ~heddit~. It will cover what you should know in order to start using a script or bot application.

*Note*: For this guide, I'm using ~generic-lens~ with ~-XOverloadedLabels~ and a lens library to access record fields. You could also impor the selectors from the modules directly.

* Table of Contents
- [[Prerequisites]]
- [[Obtain a ~Client~]]
- [[Running actions]]
  + [[Me]]
  + [[Subreddits]]
- [[More]]

* Prerequisites
** Create a Reddit account
You must have an account with Reddit in order to use the API
** Register an application
First you must [[https://old.reddit.com/prefs/apps][register an application]] with Reddit. Make sure to select a "script" application and save the client secret and client ID
** Create a user agent
You must create a unique user agent to interface with the API. Reddit rate-limits and/or bans misleading or non-descript user agents. Use the ~UserAgent~ type to create yours, which is exported by default and looks like:
#+begin_src haskell
data UserAgent = UserAgent
    { -- | The target platform
      platform :: Text
      -- | A unique application ID
    , appID    :: Text
    , version  :: Text
      -- | Your username as contact information
    , author   :: Text
    }
#+end_src

* Obtain a ~Client~
The first step is to create a ~Client~. For the sake of brevity, we will use ~newClient~, but it would be more convenient to use ~loadClient~ in real code (which also does not require directly passing your Reddit username and password).

First, create a ~PasswordFlow~ with your real Reddit username and password:

#+begin_src haskell
myPWFlow = PasswordFlow "real-username" "password123"
#+end_src

This will be used to create the correct ~AppType~, a ~ScriptApp~ which also takes the client secret obtained from Reddit:

#+begin_src haskell
myScriptApp = ScriptApp "really-secret-reddit-client-secret" myPWFlow
#+end_src

Finally, these are both used to create an ~AuthConfig~ that will stored in the ~Client~ over the course of your program:

#+begin_src haskell
myAuthConfig = AuthConfig myClientID myScriptApp myUA
#+end_src

These config is given to ~newClient~ to initialize the client:

#+begin_src haskell
main :: IO ()
main = do
     c <- newClient myAuthConfig
     ...
  where
    myAuthConfig = ...
#+end_src

* Running actions
Now that you have a Client, we will use ~runReddit~ with the client to perform any of the ~MonadReddit~ actions in the library.

** Me
First, you can use the ~getMe~ action to see that you are authenticated as the correct user
#+begin_src haskell
main :: IO ()
main = do
    ...
    me <- runReddit c getMe
    print $ me ^. #username
    ...
#+end_src

You should see your Reddit username printed.

** Subreddits
You can get the information on a single ~Subreddit~ using ~getSubreddit~:
#+begin_src haskell
main :: IO ()
main = do
    ...
    subName <- mkSubredditName "haskell"
    haskellSub <- runReddit c $ getSubreddit subName
    print $ haskellSub ^. #title
    ...
#+end_src

Which should print the title of "r/haskell"

* More
Look at the haddocks, or check the [[file:../examples][examples]] directory, to see more examples of what you can do with ~heddit~
