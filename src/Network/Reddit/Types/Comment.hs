-- |
-- Module      : Network.Reddit.Types.Comment
-- Copyright   : (c) 2021 Rory Tyler Hayford
-- License     : BSD-3-Clause
-- Maintainer  : rory.hayford@protonmail.com
-- Stability   : experimental
-- Portability : GHC
--
module Network.Reddit.Types.Comment
    {-# DEPRECATED "Use Network.Reddit.Types.Item directly" #-}
    ( Comment(..)
    , CommentID(CommentID)
    , MoreComments(..)
    , ChildComment(..)
    , WithChildren
    , WithReplies
    , commentP
    , LoadedChildren
    ) where

import           Network.Reddit.Types.Item
